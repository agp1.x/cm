# to create a library
NM=cm
!include  parms.mk

!include  objs.mk

!include  hs.mk


$(INTDIR)\$(NM).lib :   $(OBJS)    $(INTDIR)\keep_me
    $(LINK32) -lib   @<<
                               /nologo
                               /machine:I386
                               /NODEFAULTLIB:LIBC
                               /subsystem:windows 
                               /out:"$(INTDIR)/$(NM).lib" 
                               $(OBJS)
<<
    touch $(INTDIR)\keep_me


$(INTDIR)\keep_me:   $(HS) 
    touch $(S)\*.c



