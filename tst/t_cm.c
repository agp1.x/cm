//Copyright (c) 1993-2002 agp1 Software. All rights reserved
//www-page: www.i.com.ua/~agp1
//email:    agp1@smtp.ru
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
//#include <alloc.h>
#include "cm.h"

//
//
//                    �������� �ணࠬ�� �����⥬�
//                      ������������ � ���������
//
//
void    usage(char *, char *);
#define     DEFAULTFILE  "_cm.cfg"
#define     bufL         16


int main(int argc, char **argv){
int   rc;
char  buf[bufL];
char  buf2[bufL];
char  *action;
char  *rests[bufL];
 action = "�ᯮ�짮����� 䠩��";
if( argc >= 3                              ||
   (argc == 2 && toupper(*argv[1]) == 'L')
  ){
   rc = CM_useFile(DEFAULTFILE);
   if(rc == CM_OK) {
      switch(toupper(*argv[1])){

      case 'S':
       action = "�������";
           if(argc <= 4) {
                 rc     = CM_slctSctnRest(buf,bufL-1,argv[2], argv[3]);
         if(rc!=CM_OK && rc != CM_RESTWASCUT)
             printf("\n%s: %s %s",action,argv[2],argv[3]);
         else
             printf("\n=>%s<=",buf);
       }
       else {
              int i ;
         rc = CM_useSctnToSlct(argv[2]);
         for(i = 3; i < argc && rc == CM_OK; i++){
                   rc     = CM_slctRest(buf,bufL-1, argv[i]);
           if(rc!=CM_OK && rc != CM_RESTWASCUT)
               printf("\n%s: %s %s; %d",action,argv[2],argv[i], i);
           else
               printf("\n=>%s<=",buf);
         }
       }
       break;

      case 'R':
       action = "��������";
       if(argc <= 5) {
         rc = CM_rplcSctnPrmtr(argv[2], argv[3], argv[4]);
                 if(rc!=CM_OK && rc != CM_NEWSECTION)
             printf("\n%s: %s %s %s",
                  action, argv[2], argv[3], argv[4]);

       }
       else{
          int i ;
         rc = CM_useSctnToRplc(argv[2]);
                 for(i = 3; i < argc &&
                            (rc == CM_OK || rc == CM_NEWSECTION);
                                 i+=2
                     ){
           rc     = CM_rplcPrmtr( argv[i], argv[i+1]);
           if(rc!=CM_OK && rc != CM_RESTWASCUT)
               printf("\n%s: %s %s %s; %d",action,argv[2],
                  argv[i],argv[i+1], i);
         }
         rc = CM_useSctnToRplc("");

       }
       break;
      case 'E':  rc = CM_eraseSection(argv[2]);
         action = "����� ᥪ��";
         if(rc!=CM_OK)
             printf("\n%s: %s",
                  action, argv[2]);

         break;
      case 'L':
                 action = "�뤠�� ᯨ᮪ ᥪ権";
                 while((rc = CM_slctSection(buf, bufL-1)) != CM_NOTSECTION)
                 {
                    if( rc == CM_OK || rc == CM_RESTWASCUT)
                          printf("\n=>%s<= %d", buf, rc);
                    else {
                          printf("\n%s: %d", action, rc);
                          break;
                    }
                 }
         break;
      case 'D':  rc = CM_erasePrmtr(argv[2], argv[3]);
         action = "����� ��ࠬ���";
         if(rc!=CM_OK)
             printf("\n%s: %s %s",
                  action, argv[2], argv[3]);
         break;
      case 'A':  {
           int i ;
           action = "����� ��ࠬ����";
           rc = CM_useSctnToSlct( argv[2] );
           if(rc == CM_OK){
               rc = CM_countPrmtr( &i);
               for( ; i > 0  && (rc == CM_OK || rc == CM_RESTWASCUT); i--){
                 rc     = CM_slctPrmtr(buf,bufL-1, buf2, bufL-1);

                 buf [bufL-1]  = 0;  buf2[bufL-1]  = 0;

                 if(rc != CM_OK && rc != CM_RESTWASCUT)
                     printf("\n%s: %s; %d", action, argv[2], i);
                 else
                     printf("\n=>%s = %s<=",buf, buf2);
               }
           }
       }
           break;
      case 'I' : {
           int  err, l, i;
           long   sz, sz1;
           l = min( argc- 3 , bufL);
           if(argc > 3){
                 sz=0;//farcoreleft();
                  err=CCM_slctStringList(rests,argv[2],&argv[3],  l);
                  if(err != CM_OK) printf("Error\n");
                  for(i=0; i<l ;i++)
                  {
                   printf("\t%s=%s\n",argv[i+3],rests[i]);
                  }
                  CCM_freeStringList(&argv[3],rests, l);
                 sz1=0;//farcoreleft();
                 printf("Memory before: %ld, after: %ld\n",sz,sz1);
           }
           else {
                 printf("\n�� ������ ��ࠬ����");
           }
           }
           break;

      default:   usage(*argv, DEFAULTFILE);
      }
   }
   printf("\n %s code = %d", action, rc);

}
else usage(*argv, DEFAULTFILE);
return 0;
}

void usage(char *name, char *name2){
printf("\n ࠡ��� � 䠩���: %s", name2);
printf("\n �ᯮ�짮�����:");
printf("\n %s [sredali list]", name);
printf("\n \t���");
printf("\n s section parametr ...         - ����� ��ࠬ��� ᥪ樨");
printf("\n r section parametr newRest ... - �����/�������� ��ࠬ��� ᥪ樨");
printf("\n e section                      - ����� ᥪ��");
printf("\n d section parametr             - ����� ��ࠬ��� ᥪ樨");
printf("\n a section                      - ����� �� ��ࠬ���� ᥪ樨");
printf("\n l                              - ����� ᯨ᮪ ᥪ権");
printf("\n i section par1 par2 ...        - ����� ��ࠬ���� ᯨ᪮�");
printf("\n\n\t �᫨ � s ��� r ��ࠬ��஢ �����/����, � ࠧ�� �����");
exit(2);
}





